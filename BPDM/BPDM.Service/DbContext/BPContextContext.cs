﻿
using BPDM.Service.Map;
using BPDM.Service.Model;
using Microsoft.EntityFrameworkCore;


namespace BPDM.Service.DbContext
{
    public partial class BPContextContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public BPContextContext()
        {
        }

        public BPContextContext(DbContextOptions<BPContextContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PictureSetting> PictureSetting { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductSet> ProductSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=localhost;Database=BPContext;User Id=Nick;Password=mssql123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new PictureSettingMap(modelBuilder.Entity<PictureSetting>());
            new ProductMap(modelBuilder.Entity<Product>());
            new ProductSetMap(modelBuilder.Entity<ProductSet>());
        }
        
    }
}
