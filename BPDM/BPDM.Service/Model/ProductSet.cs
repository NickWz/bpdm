﻿namespace BPDM.Service.Model
{
    public partial class ProductSet
    {
        public int Id { get; set; }
        public int ProductIdMain { get; set; }
        public int ProductIdSub { get; set; }
    }
}
