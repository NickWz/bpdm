﻿namespace BPDM.Service.Model
{
    public partial class PictureSetting
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string PictureUrl { get; set; }
    }
}
