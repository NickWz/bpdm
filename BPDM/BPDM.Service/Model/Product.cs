﻿namespace BPDM.Service.Model
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Size { get; set; }
        public byte MainType { get; set; }
        public byte SubType { get; set; }
    }
}
