﻿using BPDM.Service.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BPDM.Service.Map
{
    public class PictureSettingMap
    {
        public PictureSettingMap(EntityTypeBuilder<PictureSetting> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.PictureUrl)
                .IsRequired()
                .HasMaxLength(256);
        }
    }
}