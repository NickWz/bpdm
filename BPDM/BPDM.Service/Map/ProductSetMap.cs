﻿using BPDM.Service.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BPDM.Service.Map
{
    public class ProductSetMap
    {
        public ProductSetMap(EntityTypeBuilder<ProductSet> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();
        }
    }
}