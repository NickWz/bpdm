﻿using BPDM.Service.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BPDM.Service.Map
{
    public class ProductMap
    {
        public ProductMap(EntityTypeBuilder<Product> entity)
        {
            entity.Property(e => e.Code).HasMaxLength(50);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.Size).HasMaxLength(100);
        }
    }
}