import { Component } from '@angular/core';
import { stationList } from './station-list.const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  list = stationList;
  power :any;
  factor :any;
  total = this.power * this.factor;
}
