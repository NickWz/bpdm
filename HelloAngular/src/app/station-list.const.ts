export const stationList = [
          {
            'Station': '七張站',
            'Destination': '新店站',
            'UpdateTime': '2019-12-13T15:01:22.82'
          },
          {
            'Station': '土城站',
            'Destination': '南港展覽館站',
            'UpdateTime': '2019-12-13T15:01:25.833'
          },
          {
            'Station': '大安森林公園站',
            'Destination': '北投站',
            'UpdateTime': '2019-12-13T15:01:15.56'
          },
          {
            'Station': '大橋頭站',
            'Destination': '迴龍站',
            'UpdateTime': '2019-12-13T15:01:28.473'
          },
          {
            'Station': '中山站',
            'Destination': '象山站',
            'UpdateTime': '2019-12-13T15:01:05.6'
          },
          {
            'Station': '內湖站',
            'Destination': '動物園站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '木柵站',
            'Destination': '南港展覽館站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '台北小巨蛋站',
            'Destination': '新店站',
            'UpdateTime': '2019-12-13T15:01:02.183'
          },
          {
            'Station': '台北橋站',
            'Destination': '南勢角站',
            'UpdateTime': '2019-12-13T15:01:28.473'
          },
          {
            'Station': '民權西路站',
            'Destination': '南勢角站',
            'UpdateTime': '2019-12-13T15:01:19.123'
          },
          {
            'Station': '永春站',
            'Destination': '頂埔站',
            'UpdateTime': '2019-12-13T15:01:16.757'
          },
          {
            'Station': '先嗇宮站',
            'Destination': '迴龍站',
            'UpdateTime': '2019-12-13T15:01:28.473'
          },
          {
            'Station': '西門站',
            'Destination': '南港展覽館站',
            'UpdateTime': '2019-12-13T15:01:16.757'
          },
          {
            'Station': '西門站',
            'Destination': '新店站',
            'UpdateTime': '2019-12-13T15:01:22.82'
          },
          {
            'Station': '忠孝復興站',
            'Destination': '頂埔站',
            'UpdateTime': '2019-12-13T15:01:16.757'
          },
          {
            'Station': '昆陽站',
            'Destination': '亞東醫院站',
            'UpdateTime': '2019-12-13T15:01:16.757'
          },
          {
            'Station': '松山機場站',
            'Destination': '動物園站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '芝山站',
            'Destination': '大安站',
            'UpdateTime': '2019-12-13T15:01:25.04'
          },
          {
            'Station': '信義安和站',
            'Destination': '淡水站',
            'UpdateTime': '2019-12-13T15:01:15.56'
          },
          {
            'Station': '南京復興站',
            'Destination': '松山站',
            'UpdateTime': '2019-12-13T15:01:02.183'
          },
          {
            'Station': '南港軟體園區站',
            'Destination': '南港展覽館站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '南港軟體園區站',
            'Destination': '動物園站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '徐匯中學站',
            'Destination': '南勢角站',
            'UpdateTime': '2019-12-13T15:01:20.587'
          },
          {
            'Station': '海山站',
            'Destination': '頂埔站',
            'UpdateTime': '2019-12-13T15:01:06.507'
          },
          {
            'Station': '景美站',
            'Destination': '松山站',
            'UpdateTime': '2019-12-13T15:01:02.183'
          },
          {
            'Station': '圓山站',
            'Destination': '北投站',
            'UpdateTime': '2019-12-13T15:01:25.04'
          },
          {
            'Station': '新埔站',
            'Destination': '南港展覽館站',
            'UpdateTime': '2019-12-13T15:01:16.757'
          },
          {
            'Station': '葫洲站',
            'Destination': '動物園站',
            'UpdateTime': '2019-12-13T15:01:24'
          },
          {
            'Station': '雙連站',
            'Destination': '淡水站',
            'UpdateTime': '2019-12-13T15:01:05.6'
          }
];